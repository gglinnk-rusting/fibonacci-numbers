#![allow(unused_parens)]

fn fibonacci(n: u32) -> u32 {
    match n {
        0 | 1 => n,
        _ => fibonacci(n - 1) + fibonacci(n - 2),
    }
}

fn main() {
    let n = 1;
    let fib_nbr = fibonacci(n);
    println!("Fibonacci number {} is {}", n, fib_nbr);
}
